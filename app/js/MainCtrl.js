function DataCtrl($scope, dataService) {
	$scope.catherineData = [];
	dataService.catherineData()
		.success(function(data){
			var d = data.rows;
			for(var i = 0; i < d.length; i++){
				$scope.catherineData.push({time: new Date(Date.parse(d[i].key)), amount: d[i].value});
			}
			console.log("catherineData:", $scope.catherineData);
		})
		.error(function(error){
			console.log("ERROR!", error);
		});

	$scope.allDocs = [];
	function getAllDocs() {
		dataService.allDocs()
			.success(function(data){
				var d = data.rows;
				$scope.allDocs = [];
				for(var i = 0; i < d.length; i++){
					var item = d[i].value;
					item.time = new Date(Date.parse(item.time));
					$scope.allDocs.push(item);
				}
				console.log("allDocs:", $scope.allDocs);
			})
			.error(function(err){
				console.log("ERROR!", err);
			});
	}

	$scope.deleteDoc = function (docId, docRev) {
		dataService.deleteDoc(docId, docRev)
			.success(function(data){
				console.log("Deleted successfully", data)
				getAllDocs();
			})
			.error(function(err){
				console.log("ERROR!", err);
			});
	}

	getAllDocs();
}