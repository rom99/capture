function RmVid() {
	return { 
		scope: {
			vid: '='
		},
		restrict: 'E',
		template: '<canvas class="myCanvas"></canvas>',
		replace: true,
		link: function(scope, elem, attrs) {

			var vid = scope.vid;
			var w = vid.w;
			var h = vid.h;

			var canvas = elem[0];
			canvas.width = w;
			canvas.height = h;

			var bcanvas = document.createElement('canvas');
			bcanvas.width = w;
			bcanvas.height = h

			var context = canvas.getContext('2d');
			var bcontext = bcanvas.getContext('2d');

    		function draw(video, context, bcontext, w, h){
		    	if(video.paused || video.ended) {
		    		console.log("video paused/ended");
		    		return false;
			    }

		    	bcontext.drawImage(video, 0, 0, w, h);

		    	var idata = bcontext.getImageData(0, 0, w, h);
		    	
				idata = vid.effect(idata);

		    	context.putImageData(idata, 0, 0);

		    	setTimeout(draw, 50, video, context, bcontext, w, h);
		    }

		    var video = vid.video;
		    video.addEventListener('play', function() {
				draw(vid.video, context, bcontext, w, h);	
		    });

		}
	};
}