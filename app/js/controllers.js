'use strict';

/* Controllers */
angular.module('myApp.controllers', [])
  .controller('NavCtrl', ['$scope', '$location', function ($scope, $location) {
    $scope.getClass = function (path) {
    	return $location.path() == path;
    }  
  }])
  .controller('CaptureCtrl', ['$scope', CaptureCtrl]);