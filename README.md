# Capture
Web page that captures live video from a web cam, manipulates the image on the fly and redisplays it on the page with various  simple image effects.  

This was just a fun side project.  I wanted to try out manipulating the image data from a canvas and at the same time discovered you can capture the web cam feed, direct it to a video element, then redirect the video to a canvas.  I made some fun effects but things got a bit slow after I added the 'blur' effect so I might need to rethink that one...
